import { Component } from '@angular/core';

@Component({
  selector: 'xxx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'xxx';
}
