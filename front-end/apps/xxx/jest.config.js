module.exports = {
  name: 'xxx',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/xxx',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
