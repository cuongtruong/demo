import { INestApplication, ValidationPipe } from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as helmet from 'helmet';

import { GlobalGuards } from '@xxx/auth';
import { ConfigService } from '@xxx/config';
import { AllExceptionsFilter } from '@xxx/core';

import { AppModule } from './app/app.module';

const { version, port, urlDocs, basePath } = ConfigService;

/**
 * Apply swagger
 * TODO: The app type INestApplication | any is a cheating to bypass incompatible packages after upgrading to Nestjs 6.x
 *
 * @param  {INestApplication} app   The application
 * @return {void}
 */
const useSwagger = (app: INestApplication): void => {
  const options = new DocumentBuilder()
    .setTitle('Demo API Documentation')
    .setDescription('This is an API Documentation for Demo that enables you to interact with resources')
    .setVersion(version)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup(urlDocs, app, document);
};

/**
 * Bootstraper
 *
 * @return {Promise<void>}
 */
const bootstrap = async (): Promise<void> => {
  // Register Nest application
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const reflector = app.get(Reflector);

  // User prefix as /api
  app.setGlobalPrefix(basePath);

  // Enable pipes
  app.useGlobalPipes(new ValidationPipe({
    forbidNonWhitelisted: true,
    forbidUnknownValues: true,
    transform: true,
  }));

  // Enable guards.Guards are executed after each middleware, but before any interceptor and pipe.
  app.useGlobalGuards(...GlobalGuards(reflector));

  // Use interceptors
  // app.useGlobalInterceptors(new RequestContextInterceptor());

  // User exceptions
  app.useGlobalFilters(new AllExceptionsFilter());

  // Helmet can help protect your app from some well-known web vulnerabilities by setting HTTP headers appropriately.
  // Generally, Helmet is just a collection of 14 smaller middleware functions that set security-related HTTP headers
  app.use(helmet())

  // Enable cors
  app.enableCors({ credentials: true, origin: true });

  // Apply swagger documation
  useSwagger(app);

  // App listening
  await app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
    console.log(`Swagger is available at http://localhost:${port}/${urlDocs}`);
  });
};

// Bootstrap the app
bootstrap();
