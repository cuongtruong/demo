import { Module } from '@nestjs/common';

import { ConfigModule } from '@xxx/config';
import { CacheModule } from '@xxx/core';
import { DatabaseModule } from '@xxx/database';
import { HealthCheckModule } from '@xxx/health-check';
import { AuthModule } from '@xxx/auth';

@Module({
  imports: [
    AuthModule,
    CacheModule,
    ConfigModule,
    DatabaseModule,
    HealthCheckModule,
  ],
  providers: []
})
export class AppModule { }
