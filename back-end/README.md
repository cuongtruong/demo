# xxx APIs

## Description

Source code for Demo

## Requirements

This project uses and requires knowledge on these technologies:

- JavaScript (ES6+): Programming language - <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/>
- TypeScript: typed superset of JavaScript - <https://www.typescriptlang.org/>
- NestJS: framework for building APIs - <https://docs.nestjs.com/>
- PostgreSQL: database - <https://www.postgresql.org/docs/>
- TypeORM: typed object relational mapping - <https://typeorm.io/>
- Passport: authentication for Node app - <http://www.passportjs.org/>
- JWT: token - <https://jwt.io/>
- Winston: logger - <https://github.com/winstonjs/winston>
- bcrypt: encyption - <https://www.npmjs.com/package/bcrypt>
- class-validator: validation - <https://github.com/typestack/class-validator>
- Swagger: API documentation - <https://swagger.io/>

## Environment

- NodeJS 12: JavaScript runtime - <https://nodejs.org/en/download/package-manager/>
- Yarn: Package manager - <https://classic.yarnpkg.com/en/docs/install/#windows-stable/>
- VSCode (optional): Text editor - <https://code.visualstudio.com/download/>
- Document This: Documentation comment extension - <https://marketplace.visualstudio.com/items?itemName=joelday.docthis>
- ESLint: Linter plugin - <https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint>
- Prettier - Code formatter- <https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode>
- PostgreSQL - Database - <https://www.postgresql.org/download/>

## Starting

1. Install NodeJS and Yarn
2. Install dependencies:
    - `yarn`
3. Init database:
    - Open pgAdmin
    - Go to `libs\database\src\lib\initializations`, run files
      - `01-new-role.sql`
      - `02-new-db.sql`
4. Run database migration:
    - `yarn db:migrate`
5. Run API:
    - `yarn start`
    - Swagger `http://localhost:17630/api-docs/`
​
## Other commands
### Generate database migration with [TypeORM code first](https://github.com/typeorm/typeorm)
```
 yarn db:generate -n Your_Migration_Name
```

### Run migration script to sync with database
```
 yarn db:migrate
```

### Revert database to preious version
```
 yarn db:revert
```
==============================
==============================
### NOTICE: If you run with Docker, please run the following commands:
#### Create folders for mapping upload files/public material between host and container (Required)
```
mkdir -p /apps/xxx-api/public
mkdir -p /apps/xxx-api/upload
```
#### View logs
- Currently system only logs errors and integration parts. You can use to view/visualize logs or using following command:
    ```
    # View list of docker volumes. You would see a volume named "{{xxx}}xxx-api-logs"
    docker volume ls
    ​
    # Inspect log volume with to get Mountpoint
    # You would see Mountpoint: /var/lib/docker/volumes/{{xxx}}xxx-api-logs/_data
    docker inspect {{xxx}}xxx-api-logs
    ​
    # cd to Mountpoint to view log files
    cd /var/lib/docker/volumes/{{xxx}}xxx-api-logs/_data
    ls -al
    ```
​
## Common error codes:
  - 400 - invalid params: User input wrong param. Ex: phoneNumber=1234asdf.
  - 403 - access denied. User accessed forbiden resources.
  - 404 - not found. User accessed not-existed resource/path.
  - 500 - internal server error. Exception is occurred that have not been handled. You should resolve this issue.
​
​