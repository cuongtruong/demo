/* eslint-disable no-undef */
module.exports = {
  'name': 'default',
  'type': 'postgres',
  'host': process.env.DB_HOST || 'localhost',
  'port': Number(process.env.DB_PORT) || 5432,
  'username': process.env.DB_USER || 'demoadmin',
  'password': process.env.DB_PASSWORD || '(3McstX//2+kbg<j',
  'database': process.env.DB_NAME || 'demo',
  'synchronize': process.env.ORM_SYNCHRONIZE === 'true' || false,
  'logging': process.env.ORM_LOGGING === 'true' || true,
  'cache': process.env.ORM_CACHE === 'true' || false,
  'schema': process.env.DB_SCHEMA || 'public',
  'entities': [
    'libs/**/*.entity.ts'
  ],
  'migrations': [
    'libs/database/src/lib/migrations/**/*.ts'
  ],
  'subscribers': [
    'libs/database/src/lib/subscribers/**/*.ts'
  ],
  'cli': {
    'entitiesDir': 'libs/database/src/lib/entities',
    'migrationsDir': 'libs/database/src/lib/migrations',
    'subscribersDir': 'libs/database/src/lib/subscribers'
  }
};
