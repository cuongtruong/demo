export interface DatabaseOptions {
  type: string;
  host: string;
  port: number;
  username: string;
  password: string;
  database: string | Uint8Array;
  synchronize: boolean;
  schema: string;
  logging: boolean;
  cache: boolean;
}

export interface CorsOptions {
  origin: string | string[];
  headers: string | string[];
  methods: string | string[];
  expose: string | string[];
}

export interface AppSessionOptions {
  secret: string;
  expiresIn: number;
}

export interface ENV {
  // Common
  VERSION: string;
  HOST: string;
  PORT: number;
  NODE_ENV: string;
  APP_ID: string;
  APP_SECRET: string;

  // Security
  APP_SESSION_SECRET: string;
  APP_SESSION_EXPIRES_IN: number;

  // Database
  DB_NAME: string;
  DB_USER: string;
  DB_PASSWORD: string;
  DB_HOST: string;
  DB_PORT: number;
  DB_SCHEMA: string;

  // TYPEORM
  TYPEORM_CONNECTION: string;
  TYPEORM_HOST: string;
  TYPEORM_PORT: number;
  TYPEORM_USERNAME: string;
  TYPEORM_PASSWORD: string;
  TYPEORM_DATABASE: string;
  TYPEORM_SYNCHRONIZE: boolean;
  TYPEORM_LOGGING: boolean;
  TYPEORM_CACHE: boolean;
  TYPEORM_SCHEMA: string;
  TYPEORM_ENTITIES: string;
  TYPEORM_SUBSCRIBERS: string;
  TYPEORM_MIGRATIONS: string;
  TYPEORM_ENTITIES_DIR: string;
  TYPEORM_MIGRATIONS_DIR: string;
  TYPEORM_SUBSCRIBERS_DIR: string;

  // CORS
  CORS_ORIGIN: string;
  CORS_HEADERS: string;
  CORS_METHODS: string;
  CORS_EXPOSE: string;

  // Urls
  BASE_PATH: string;
  URL_DOCS: string;

  // Logger
  LOG_LEVEL: string;
  LOG_SERVICE_NAME: string;
  LOG_APPENDERS: any;
  LOG_PATH: string;
}
