# health-check

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test health-check` to execute the unit tests via [Jest](https://jestjs.io).
