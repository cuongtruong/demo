import { Test } from '@nestjs/testing';

import { HealthCheckService } from './health-check.service';

describe('HealthCheckService', () => {
  let service: HealthCheckService;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      providers: [HealthCheckService]
    }).compile();

    service = module.get<HealthCheckService>(HealthCheckService);
  });

  describe('getData', () => {
    it('should return "Welcome to test!"', () => {
      expect(service.getData()).toEqual({ message: 'Welcome to test!' });
    });
  });
});
