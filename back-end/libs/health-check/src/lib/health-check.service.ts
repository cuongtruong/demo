import { Injectable } from '@nestjs/common';

import { ConfigService } from '@xxx/config';

@Injectable()
export class HealthCheckService {
  /**
   * Get data test
   * @return {string}     The data test
   */
  getData(): { message: string } {
    return { message: 'Welcome to test!' };
  }

  /**
   * Get service's version
   *
   * @return {string}     The service's version
   */
  getVersion(): string {
    return ConfigService.version;
  }
}
