import { Test, TestingModule } from '@nestjs/testing';

import { HealthCheckController } from './health-check.controller';
import { HealthCheckService } from './health-check.service';

describe('HealthCheckController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [HealthCheckController],
      providers: [HealthCheckService]
    }).compile();
  });

  describe('getData', () => {
    it('should return "Welcome to test!"', () => {
      const appController = app.get<HealthCheckController>(HealthCheckController);
      expect(appController.getData()).toEqual({ message: 'Welcome to test!' });
    });
  });
});
