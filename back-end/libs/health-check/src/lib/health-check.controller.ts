import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { ConfigService } from '@xxx/config';
import { Public } from '@xxx/core';

import { HealthCheckService } from './health-check.service';

@ApiTags('HealthCheck')
@Controller('health-check')
export class HealthCheckController {
  /**
   * Constructor
   */
  constructor(private readonly healthCheckService: HealthCheckService) {}

  /**
   * Get data test
   *
   * @return {string}   The data test
   */
  @Get()
  @Public()
  getData() {
    return this.healthCheckService.getData();
  }

  /**
   * Get service's version
   *
   * @return {string}     The service's version
   */
  @Get('version')
  @Public()
  @ApiOperation({ summary: 'Retrieves service\'s version' })
  @ApiOkResponse({ description: 'Returns service\'s version' })
  async getVersion(): Promise<string> {
    return ConfigService.version;
  }
}
