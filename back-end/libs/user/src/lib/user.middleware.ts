import { BadRequestException, Injectable, NestMiddleware, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { isUUID } from '@xxx/core';
import { User } from '@xxx/database';

export const MIDDLEWARE_KEY_USER = 'foundUser';

@Injectable()
export class UserMiddleware implements NestMiddleware {
  /**
   * Constructor
   */
  constructor(@InjectRepository(User) private readonly userRepository: Repository<User>) {
  }

  async use(req: any, res: any, next: () => void) {
    // if this middleware is misapplied to a route without ID, params.id would be null
    const { id = '' } = req.params || {};

    if (!id) {
      throw new BadRequestException('UserId is required');
    }

    if (isUUID(id)) {
      const user = await this.userRepository.findOne(id);

      if (!user) {
        throw new NotFoundException('User not found.');
      }

      req[MIDDLEWARE_KEY_USER] = user;
    }

    next && next();
  }
}