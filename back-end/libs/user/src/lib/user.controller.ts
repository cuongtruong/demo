import { BadRequestException, Body, Controller, Delete, Get, NotFoundException, Param, ParseUUIDPipe, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiParam, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

import { BaseController, Roles, Public } from '@xxx/core';
import { Role } from '@xxx/database';

import { UserConverter } from './user.converter';
import { EditUserRequest, EditUserProfileRequest, UserPaginatedRequest, UserResponse, UserPaginatedResponse, CreateUserRequest } from './user.dto';
import { UserGuard } from './user.guard';
import { UserService } from './user.service';

@ApiTags('User')
@Controller('users')
export class UserController extends BaseController {
  /**
   * Constructor
   */
  constructor(
    private readonly service: UserService,
    private readonly converter: UserConverter
  ) {
    super();
  }

  /**
   * Add new user
   *
   * @param  {UserResponse} request     The user info
   * @return {Promise<UserResponse>}
   */
  @Public()
  @Post()
  @ApiOperation({ summary: 'Create new user' })
  @ApiCreatedResponse({ type: UserResponse, description: 'Returns user info' })
  async add(@Body() request: CreateUserRequest): Promise<UserResponse> {
    const entity = await this.service.add(request);
    const response = this.converter.toDTO(entity);

    return response;
  }

  /**
   * Get all users
   *
   * @param  {Request} req        The request
   * @return {Promise<UserPaginatedResponse>}
   */
  @Roles(Role.sysadmin, Role.admin)
  @Get()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Retrieves all users' })
  @ApiOkResponse({ type: UserPaginatedResponse, description: 'Returns all users' })
  async getAll(@Query() filters: UserPaginatedRequest): Promise<UserPaginatedResponse> {
    const { items, ...data } = await this.service.getAll(filters);
    const dtos = this.converter.toDTOs(items);

    return { ...data, items: dtos };
  }

  /**
   * Get user by id
   *
   * @param  {Request} req        The request
   * @param  {string} id          The user id
   * @return {Promise<UserResponse>}
   */
  @UseGuards(UserGuard)
  @Get(':id')
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: 'string', description: 'The user id', required: true })
  @ApiOperation({ summary: 'Retrieves user info' })
  @ApiOkResponse({ type: UserResponse, description: 'Returns a specific user' })
  async get(@Param('id', new ParseUUIDPipe()) id: string): Promise<UserResponse> {
    const entity = await this.service.get(id);

    if (!entity) throw new NotFoundException();

    const response = this.converter.toDTO(entity);

    return response;
  }

  /**
   * Update user by id
   *
   * @param  {EditUserRequest} request     The user info
   * @return {Promise<UserResponse>}
   */
  @Roles(Role.sysadmin, Role.admin)
  @UseGuards(UserGuard)
  @Put(':id')
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: 'string', description: 'The user id', required: true })
  @ApiOperation({ summary: 'Updates user info' })
  @ApiOkResponse({ type: UserResponse, description: 'Returns user info' })
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() request: EditUserRequest): Promise<UserResponse> {
    if (id && request) {
      const entity = await this.service.update({ ...request, id });
      const response = this.converter.toDTO(entity);

      return response;
    }
    else {
      throw new BadRequestException();
    }
  }

  /**
   * Update user profile by id
   *
   * @param  {EditUserProfileRequest} request     The user profile info
   * @return {Promise<UserResponse>}
   */
  @UseGuards(UserGuard)
  @Put(':id/profile')
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: 'string', description: 'The user id', required: true })
  @ApiOperation({ summary: 'Updates user profile' })
  @ApiOkResponse({ type: UserResponse, description: 'Returns user info' })
  async updateProfile(@Param('id', new ParseUUIDPipe()) id: string, @Body() request: EditUserProfileRequest): Promise<UserResponse> {
    if (id && request) {
      const entity = await this.service.update({ ...request, id });
      const response = this.converter.toDTO(entity);

      return response;
    }
    else {
      throw new BadRequestException();
    }
  }

  /**
   * Delete user by id
   *
   * @param  {Request} req        The request
   * @return {Promise<UserResponse>}
   */
  @Roles(Role.sysadmin, Role.admin)
  @UseGuards(UserGuard)
  @Delete(':id')
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: 'string', description: 'The user id', required: true })
  @ApiOperation({ summary: 'Deletes a user' })
  @ApiOkResponse({ type: UserResponse, description: 'Returns deleted user' })
  async delete(@Param('id', new ParseUUIDPipe()) id: string | any): Promise<UserResponse> {
    const entity = await this.service.get(id);

    if (!entity) throw new NotFoundException();

    const deletedEntity = await this.service.remove(entity);
    const response = this.converter.toDTO(deletedEntity);

    return response;
  }
}
