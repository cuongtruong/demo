import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Pagination, paginate } from 'nestjs-typeorm-paginate';
import { In, Repository, SelectQueryBuilder } from 'typeorm';

import { CacheService, encrypt, isUUID, ContextService, SortDir } from '@xxx/core';
import { Feature, Role, User, AuthProvider } from '@xxx/database';

import { EditUserRequest, UserPaginatedRequest, UserResponse, CreateUserRequest } from './user.dto';
import { UserQueryBuilder } from './user.interface';

@Injectable()
export class UserService {
  /**
   * Constructor
   */
  constructor(
    @InjectRepository(Feature) private readonly featureRepository: Repository<Feature>,
    @InjectRepository(User) private readonly  userRepository: Repository<User>,
    private readonly contextService: ContextService,
    private readonly cacheService: CacheService
  ) { }


  /**
   * Create new user
   *
   * @param  {CreateUserRequest}  The create user request
   * @return {Promise<User>}      The user info
   */
  async add(user: CreateUserRequest): Promise<User> {
    return this.save(user);
  }

  /**
   * Get all users except SystemAdmin
   *
   * @param  {UserPaginatedRequest} filters      The filter options
   * @return {Promise<Pagination<User>>}         The paginated list of users
   */
  async getAll(filters: UserPaginatedRequest): Promise<Pagination<User>> {
    return await this.paginate({ filters });
  }

  /**
   * Get user by id
   *
   * @param  {string} id        The user id
   * @return {Promise<User>}    The user info
   */
  async get(id: string): Promise<User> {
    if (isUUID(id)) {
      return await this.eagerWithRelations({ where: { id } }).getOne();
    }

    throw new BadRequestException('Id must be UUID');
  }

  /**
   * Get user by iusername
   *
   * @param  {string} username  The username
   * @return {Promise<User>}    The user info
   */
  async getByUsername(username: string): Promise<User> {
    // TODO: handle cmphe
    // return await this.userRepository.findOne({ where: { username }, cache: { id: `users_${username}`, milliseconds: 5 * 60 * 1000 } });
    return await this.eagerWithRelations({ where: { username } }).getOne();
  }

  /**
   * Update user info
   *
   * @param  {Partial<UserResponse>} user  The user info
   * @return {Promise<User>}          The updated user info
   */
  async update(user: Partial<UserResponse>): Promise<User> {
    return await this.save(user);
  }

  /**
   * Delete user
   *
   * @param  {User} user        The user info
   * @return {Promise<User>}    The deleted user
   */
  async remove(user: User): Promise<User> {
    return await this.userRepository.remove(user);
  }

  /**
   * Save user info
   *
   * @param  {Partial<EditUserRequest & CreateUserRequest>} user    The user info
   * @return {User}
   */
  private async save(user: Partial<EditUserRequest & CreateUserRequest>): Promise<User> {
    const { id, username, name, avatar, role, active, provider, externalId } = user;
    const existingUser = await this.userRepository.findOne({ where: { username } });

    if (existingUser && existingUser.id !== id) throw new BadRequestException('Username already in used');

    const features: Feature[] = user.provider == AuthProvider.persNbr
      ? await this.featureRepository.find({ where: { code: In(['blueRewards']) } })
      : [];
    const { salt = undefined, hashedValue: passwordHash = undefined } = user.password ? await encrypt(user.password) : {};
    const currentLoggedInUser = this.contextService.user;

    console.log('currentLoggedInUser', currentLoggedInUser);
    const entity = new User({
      id,
      username,
      name,
      role,
      active,
      provider,
      externalId,
      salt,
      passwordHash,
      features,
      validFrom: existingUser?.validFrom,
      avatar: existingUser?.avatar || avatar,
      createdBy: existingUser?.name || currentLoggedInUser?.name,
      createdById: existingUser?.id || currentLoggedInUser?.sub,
      updatedBy: currentLoggedInUser?.name,
      updatedById: currentLoggedInUser?.sub,
    });

    // Clear cache data
    this.cacheService.del(`user:${username}`);

    return await this.userRepository.save(entity);
  }

  /**
   * Generate query builder with Relation loaded eagerly
   *
   * @return {SelectQueryBuilder<User>}
   */
  private eagerWithRelations({ select, where }: UserQueryBuilder): SelectQueryBuilder<User> {
    // return await this.userRepository.findOne(id, { relations: ['features'] }); // This way will hit 2 times to db
    // We write manual query to reduce round trip to db (1 hit)
    const queryBuilder = this.userRepository
      .createQueryBuilder('user')
      .leftJoin('user.features', 'feature')
      .addSelect('feature.code');

    // Select fields
    (select || []).forEach(field => queryBuilder.addSelect(field));

    // Where conditions
    where && queryBuilder.where(where);

    // Only SysAdmin can get his own data
    this.contextService.user.role !== Role.sysadmin && queryBuilder.andWhere('role != :role', { role: Role.sysadmin });

    return queryBuilder;
  }

  /**
   * Generate query builder with Feature loaded eagerly
   *
   * @return {SelectQueryBuilder<User>}
   */
  private paginate({ filters }: UserQueryBuilder & { filters?: UserPaginatedRequest }): Promise<Pagination<User>> {
    const {
      search = '',
      page = 1,
      limit = 10,
      sortBy = 'updatedAt',
      sortDir = SortDir.DESC,
      role,
      active
    } = filters;
    let whereQuery = '1 = 1';

    // Search by term
    if (search) {
      const likeIgnoreCaseOperator = ` ILIKE '%${search}%'`;

      whereQuery += ` AND ("name" ${likeIgnoreCaseOperator} OR "username" ${likeIgnoreCaseOperator} OR "email" ${likeIgnoreCaseOperator})`;
    }

    // Only SysAdmin can get his own data
    if (this.contextService.user.role !== Role.sysadmin) {
      whereQuery += ` AND "role" != '${Role.sysadmin}'`;
    }

    // Search by role
    if (!!role) {
      whereQuery += ` AND "role" = '${role}'`;
    }

    // Search by status
    if (!!active) {
      whereQuery += ` AND "active" = ${active}`;
    }

    return paginate<User>(
      this.userRepository,
      { page, limit },
      {
        where: whereQuery,
        order: { [sortBy]: sortDir.toLocaleUpperCase() }
      });
  }

  //   /**
  //  * Generate query builder with Feature loaded eagerly
  //  *
  //  * @return {SelectQueryBuilder<User>}
  //  */
  //   private paginate({ select, where, filters }: UserQueryBuilder & { filters?: FilterUserDTO }): Promise<Pagination<User>> {
  //     const { search = '', page = 1, limit = 10, sortBy = 'updatedAt', sortDir = 'DESC', role, active } = filters;
  //     const whereConditions = where ? [where] : [];
  //     const selectFields = select ? [select] : [];

  //     if (search) {
  //       const likeIgnoreCaseOperator = Raw(alias => `${alias} ILIKE '%${search}%'`);

  //       whereConditions.push({ name: likeIgnoreCaseOperator });
  //       whereConditions.push({ username: likeIgnoreCaseOperator });
  //       whereConditions.push({ email: likeIgnoreCaseOperator });
  //     }

  //     // Only SysAdmin can get his own data
  //     this.currentUser.role !== Role.sysadmin && whereConditions.push({ role: Not(Role.sysadmin) });

  //     !!role && whereConditions.push({ role });
  //     !!active && whereConditions.push({ active });

  //     return paginate<User>(
  //       this.userRepository,
  //       { page, limit },
  //       {
  //         where: whereConditions,
  //         order: { [sortBy]: sortDir.toLocaleUpperCase() }
  //       });
  //   }
}
