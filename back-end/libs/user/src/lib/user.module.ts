import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ContextService } from '@xxx/core';
import { Feature, User } from '@xxx/database';

import { UserController } from './user.controller';
import { UserConverter } from './user.converter';
import { UserMiddleware } from './user.middleware';
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Feature, User]),
  ],
  controllers: [UserController],
  providers: [UserService, UserConverter, ContextService],
  exports: [UserService, UserConverter]
})
export class UserModule implements NestModule {
  /**
   * Configure middlewares
   *
   * @param  {MiddlewareConsumer} consumer    The middleware consumer
   * @return {void}
   */
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(UserMiddleware)
      .forRoutes({
        path: 'users/:id', method: RequestMethod.ALL
      });
  }
}