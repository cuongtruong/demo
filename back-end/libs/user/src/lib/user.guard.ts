import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

import { ContextService, UserClaims } from '@xxx/core';
import { ADMINISTRATOR_ROLES } from '@xxx/database';

import { MIDDLEWARE_KEY_USER } from './user.middleware';

@Injectable()
export class UserGuard implements CanActivate {
  /**
   * Validate the request
   * The context inherits from ArgumentsHost.
   * The ArgumentsHost is a wrapper around arguments that have been passed to the original handler,
   * and it contains different arguments array under the hood based on the type of the application
   *
   * @param  {ExecutionContext} context   The context
   * @return {boolean}
   */
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const user = request[MIDDLEWARE_KEY_USER];
    const currentUser = request[ContextService.KEY_USER] as UserClaims;

    return currentUser
      && user
      && (ADMINISTRATOR_ROLES.includes(currentUser.role) || (currentUser && currentUser.sub === user.id));
  }
}