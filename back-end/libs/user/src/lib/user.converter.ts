import { Injectable } from '@nestjs/common';

import { BaseConverter } from '@xxx/core';
import { User } from '@xxx/database';

import { UserResponse } from './user.dto';

@Injectable()
export class UserConverter extends BaseConverter<UserResponse, User> {}