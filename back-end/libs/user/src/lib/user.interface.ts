import { Brackets, ObjectLiteral, SelectQueryBuilder } from 'typeorm';

import { User } from '@xxx/database';

export interface UserQueryBuilder {
  select?: string[];
  where?: string | Brackets | ObjectLiteral | ObjectLiteral[] | ((qb: SelectQueryBuilder<User>) => string);
}