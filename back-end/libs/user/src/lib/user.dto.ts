import { ApiProperty, ApiResponseProperty } from '@nestjs/swagger';
import { IsNotEmpty, Allow } from 'class-validator';

import { AuditableResponse, BasePaginatedRequest, PaginatedResponse, BaseRequest } from '@xxx/core';
import { Role, User, AuthProvider } from '@xxx/database';

const ALLOWED_ROLES = Object.keys(Role).filter(x => x != Role.sysadmin);

export class EditUserProfileRequest extends BaseRequest {
  @IsNotEmpty()
  @ApiProperty({ example: '0909090909' })
  username: string;

  @IsNotEmpty()
  @ApiProperty({ example: 'Win Truong' })
  name: string;
}

export class EditUserRequest extends EditUserProfileRequest {
  @IsNotEmpty()
  @ApiProperty({ example: Role.user, enum: ALLOWED_ROLES })
  role: Role;

  @ApiProperty({ example: AuthProvider.phone, enum: Object.keys(AuthProvider) })
  provider: AuthProvider;

  @ApiProperty({ example: 'https://bit.ly/2TV3qqQ' })
  avatar: string;

  @ApiProperty({ example: true, required: false, default: true })
  active: boolean;

  @ApiProperty({ example: 'ac741f54ca46c85f', required: false })
  externalId: string;
}

export class CreateUserRequest {
  @IsNotEmpty()
  @ApiProperty({ example: '0909090909' })
  username: string;

  @IsNotEmpty()
  @ApiProperty({ example: 'Win Truong' })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ example: Role.user, enum: ALLOWED_ROLES })
  role: Role;

  @ApiProperty({ example: AuthProvider.phone, enum: Object.keys(AuthProvider) })
  provider: AuthProvider;

  @ApiProperty({ example: 'Y0urP@ssw0rD', required: false })
  password?: string;

  @ApiProperty({ example: true, required: false, default: true })
  active: boolean;

  @ApiProperty({ example: 'ac741f54ca46c85f', required: false })
  externalId: string;
}

export class UserResponse extends AuditableResponse {
  @ApiResponseProperty({ example: '0909090909' })
  username: string;

  @ApiResponseProperty({ example: 'Win Truong' })
  name: string;

  @ApiResponseProperty({ example: Role.user, enum: ALLOWED_ROLES })
  role: Role;

  @ApiResponseProperty({ example: AuthProvider.phone })
  provider: AuthProvider;

  @ApiResponseProperty({ example: true })
  active: boolean;

  @ApiResponseProperty({ example: 'ac741f54ca46c85f' })
  externalId: string;

  @ApiResponseProperty({ example: ['blueRewards'] })
  featureCodes: string[];
}

export class UserPaginatedResponse extends PaginatedResponse(UserResponse) { }

export class UserPaginatedRequest extends BasePaginatedRequest<User> {
  @Allow()
  @ApiProperty({ description: 'Type something to search', required: false })
  search: string;

  @Allow()
  @ApiProperty({ example: Role.user, enum: ALLOWED_ROLES, required: false })
  role: Role;

  @Allow()
  @ApiProperty({ example: true, enum: ['true', 'false'], required: false })
  active: boolean;
}
