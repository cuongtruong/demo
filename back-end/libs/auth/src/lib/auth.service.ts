import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { compare } from 'bcryptjs';
import { Repository } from 'typeorm';

import { Cache, CacheService, encrypt } from '@xxx/core';
import { User, AuthProvider, Role } from '@xxx/database';

import { ChangePasswordRequest, LoginRequest, JwtPayload, TokensResponse } from './auth.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
    private readonly cacheService: CacheService,
  ) { }

  /**
   * Create token
   *
   * @param  {LoginRequest} loginRequest    The credentials info
   * @return {Promise<TokensResponse>}
   */
  async login(loginRequest: LoginRequest): Promise<TokensResponse> {
    try {
      const { id, username, name, role } = await this.tryLogin(loginRequest);
      const accessToken = this.jwtService.sign({ username, name, role }, { subject: id });

      return { accessToken };
    }
    catch (error) {
      throw new UnauthorizedException('Invalid credentials');
    }
  }

  /**
   * Change password
   *
   * @param  {ChangePasswordRequest} dto    The change password info
   * @return {Promise<boolean>}
   */
  async changePassword({ username, oldPassword, password }: ChangePasswordRequest): Promise<boolean> {
    try {
      if (username && password !== oldPassword) {
        const user = await this.tryLogin({ username, password: oldPassword });
        const { salt, hashedValue: passwordHash } = await encrypt(password);
        const entity = await this.userRepository.save({ ...user, salt, passwordHash });

        // Clear cache data after changed password
        this.cacheService.del(`user:${user.username}`);

        return !!entity;
      }
    }
    catch (error) {
      throw new BadRequestException('The provided passwords are invalid');
    }

    throw new BadRequestException('The provided passwords are invalid');
  }

  /**
   * Verify token
   *
   * @param  {string} token   The token
   * @return {Promise<any>}
   */
  async verifyToken(token: string): Promise<any> {
    return await this.jwtService.verifyAsync((token || '').replace('Bearer ', ''));
  }

  /**
   * Validate payload info
   *
   * @param  {JwtPayload} payload   The payload info
   * @return {Promise<User>}
   */
  async validatePayload({ username }: JwtPayload): Promise<User> {
    return await this.getUserByUsername(username);
  }

  /**
   * Perform login
   *
   * @param  {string} username      The username
   * @param  {string} password      The password
   * @return {Promise<User>}        The user info
   */
  private async tryLogin({ username, password }: LoginRequest): Promise<User> {
    // Get user with features loaded eagerly
    const user = await this.getUserByUsername(username);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    const isValid = user.active && await compare(password, user.passwordHash);

    if (!isValid) {
      throw new UnauthorizedException();
    }

    return user;
  }

  /**
   * Get user by username
   *
   * @param  {string} username    The user name
   * @return {Promise<User>}
   */
  @Cache({ keyPrefix: 'user', keyByParams: true })
  private async getUserByUsername(username: string): Promise<User> {
    return await this.userRepository
      .createQueryBuilder('user')
      .leftJoin('user.features', 'feature')
      .addSelect('feature.code')
      .where({ username })
      .getOne();
  }
  // TODO: Handle logout with clean up tokens: https://github.com/abouroubi/nestjs-auth-jwt/blob/master/src/auth/auth.service.ts
}
