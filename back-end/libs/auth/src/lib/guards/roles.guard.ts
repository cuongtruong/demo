import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

import { DECORATOR_TOKEN_ROLES, ContextService } from '@xxx/core';
import { Role, User } from '@xxx/database';

@Injectable()
export class RolesGuard implements CanActivate {
  /**
   * Constructor
   */
  constructor(private readonly reflector: Reflector) { }

  /**
   * Validate the request
   * The context inherits from ArgumentsHost.
   * The ArgumentsHost is a wrapper around arguments that have been passed to the original handler,
   * and it contains different arguments array under the hood based on the type of the application
   *
   * @param  {ExecutionContext} context   The context
   * @return {boolean}
   */
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const roles = this.reflector.get<string[]>(DECORATOR_TOKEN_ROLES, context.getHandler())
      || this.reflector.get<string[]>(DECORATOR_TOKEN_ROLES, context.getClass());

    if (!roles) { return true; }

    const request = context.switchToHttp().getRequest();
    const user = request[ContextService.KEY_USER] as User;

    return user && (
      user.role === Role.sysadmin
      || roles.includes(user.role)
    );
  }
}