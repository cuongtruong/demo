import { Reflector } from '@nestjs/core';

import { JwtAuthGuard } from './jwt.guard';
import { PermissionsGuard } from './permissions.guard';
import { RolesGuard } from './roles.guard';

export const GlobalGuards = (reflector: Reflector) => {
  return [
    new JwtAuthGuard(reflector),
    new RolesGuard(reflector),
    new PermissionsGuard(reflector)
  ];
};

export * from './jwt.guard';
export * from './permissions.guard';
export * from './roles.guard';
