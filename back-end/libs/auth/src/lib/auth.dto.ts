import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Matches, MinLength } from 'class-validator';

import { REGEX_PASSWORD } from '@xxx/core';

export class JwtPayload {
  @ApiProperty({ required: true, example: 'user1' })
  @IsNotEmpty()
  username: string;
}

export class LoginRequest {
  @ApiProperty({ example: '0909090909' })
  @IsString()
  readonly username: string;

  @ApiProperty({ example: 'Y0urP@ssw0rD' })
  @IsString()
  readonly password: string;
}

export class ChangePasswordRequest {
  @IsString()
  @ApiProperty({ example: '0909090909' })
  readonly username: string;

  @IsString()
  @ApiProperty({ example: 'Y0urP@ssw0rD' })
  readonly oldPassword: string;

  @IsString()
  @MinLength(8)
  @Matches(REGEX_PASSWORD)
  @ApiProperty({ example: 'Y0urN3wP@ssw0rD' })
  readonly password: string;
}

export class TokensResponse {
  // tslint:disable-next-line: max-line-length
  @ApiProperty({ example: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjYwOThkMzQ0LTlhY2YtNGZlMS1iN2FmLTg5ODUzNTljYmRhZSIsImNyZWF0ZWRBdCI6IjIwMTktMDQtMDJUMDc6MDE6MzYuNjg2WiIsInVwZGF0ZWRBdCI6IjIwMTktMDQtMDJUMTE6MTA6NDQuODQzWiIsInVzZXJuYW1lIjoiYWdlbnQ2Iiwicm9sZSI6InVzZXIiLCJmaXJzdE5hbWUiOiJBZ2VudCIsImxhc3ROYW1lIjoiIzYiLCJhY3RpdmUiOnRydWUsImZlYXR1cmVJZHMiOlsiM2M2NTJmZTItYTdiOC00YTVkLTg4NmMtYmU1NDY5MWNiZGMwIl0sImlhdCI6MTU1NDI5Mzc0MCwiZXhwIjoxNTU0Mjk3MzQwLCJpc3MiOiI2YTgwZGVlOS0zMDRiLTRiNTYtOTMyOS0wMTliODgyN2E0OTgifQ.9AtqOMvmYADXX9n6WwLNH4Z7IEBYxOYkQhWOA9hx_eI' })
  accessToken: string;
}
