import { HttpModule, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ConfigService } from '@xxx/config';
import { User } from '@xxx/database';

import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards';
import { JwtStrategy } from './strategies';

const GUARDS = [
  JwtAuthGuard,
];

const STRATEGIES = [
  JwtStrategy,
];

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: ConfigService.appSession.secret,
      signOptions: {
        expiresIn: ConfigService.appSession.expiresIn,
        issuer: ConfigService.appId,
      }
    }),
  ],
  providers: [
    AuthService,
    ...GUARDS,
    ...STRATEGIES,
  ],
  exports: [
    AuthService,
    ...GUARDS,
  ]
})
export class BaseAuthModule { }

@Module({
  imports: [
    HttpModule,
    BaseAuthModule
  ],
  controllers: [AuthController],
  exports: [BaseAuthModule]
})
export class AuthModule { }

