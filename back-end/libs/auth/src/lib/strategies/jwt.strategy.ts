import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

import { ConfigService } from '@xxx/config';
import { User } from '@xxx/database';

import { JwtPayload } from '../auth.dto';
import { AuthService } from '../auth.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  /**
   * Constructor
   */
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderWithScheme('jwt'),
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        ExtractJwt.fromUrlQueryParameter('access_token'),
      ]),
      secretOrKey: ConfigService.appSession.secret,
      passReqToCallback: true
    });
  }

  /**
   * Vavlidate the payload
   *
   * @param  {JwtPayload} payload   The payload info
   * @return {type} {description}
   */
  async validate(req: any, payload: JwtPayload): Promise<User> {
    // // Little hack but ¯\_(ツ)_/¯
    // const self: any = this;
    // const token = self._jwtFromRequest(req);

    // We can now use this token to check it against black list
    // @todo: check against black list :D
    const user = await this.authService.validatePayload(payload);

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}