import { Body, Controller, Get, Headers, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags, ApiCreatedResponse } from '@nestjs/swagger';

import { BaseController, Public } from '@xxx/core';

import { ChangePasswordRequest, LoginRequest, TokensResponse } from './auth.dto';
import { AuthService } from './auth.service';

@ApiTags('Auth')
@Controller('auth')
export class AuthController extends BaseController {
  /**
   * Constructor
   */
  constructor(private readonly authService: AuthService) {
    super();
  }

  @Public()
  @Post('login')
  @ApiOperation({ summary: 'Log in to system' })
  @ApiCreatedResponse({ type: TokensResponse, description: 'The tokens { accessToken } info' })
  public async login(@Body() loginRequest: LoginRequest): Promise<TokensResponse> {
    return await this.authService.login(loginRequest);
  }

  @Get('verify')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Verify token' })
  @ApiOkResponse({ type: Boolean, description: 'Indicates the token is valid or not' })
  public async verify(@Headers('authorization') token: string): Promise<boolean> {
    return await this.authService.verifyToken(token);
  }

  @Post('change-password')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Change password' })
  @ApiOkResponse({ type: Boolean, description: 'True if password changed successfully' })
  public async changePassword(@Body() changePasswordRequest: ChangePasswordRequest): Promise<boolean> {
    return await this.authService.changePassword(changePasswordRequest);
  }
}
