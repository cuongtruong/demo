export const PATTERN_PASSWORD = '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$';
export const REGEX_PASSWORD = new RegExp(PATTERN_PASSWORD);
