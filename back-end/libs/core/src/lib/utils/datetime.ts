/**
 * Get start of day
 *
 * @param  {string | Date} value    The value to be parsed
 * @return {Date}
 */
export const startOfDay = (value: string | Date): Date => {
  return new Date(new Date(value).setHours(0, 0, 0, 0));
};

/**
 * Get end of day
 *
 * @param  {string | Date} value    The value to be parsed
 * @return {Date}
 */
export const endOfDay = (value: string | Date): Date => {
  return new Date(new Date(value).setHours(23, 59, 59, 999));
};

/**
 * Get current date string
 * @param  {string} delimiter     The delimiter
 * @return {string}
 */
export const getCurrentDate = (delimiter: string = '/'): string => {
  return new Date().toJSON().slice(0, 10).replace(/-/g, delimiter);
};

/**
 * Get date string format dd/mm/yyyy
 * @param  {string} delimiter     The delimiter
 * @return {string}
 */
export const convertDateToString = (date: Date, delimiter: string = '/'): string => {
  const parseDate = new Date(date);

  const dd = parseDate.getDate();
  const mm = parseDate.getMonth() + 1;
  const yyyy = parseDate.getFullYear();

  return `${dd < 10 ? `0${dd}` : dd}${delimiter}${mm < 10 ? '0' + mm : mm}${delimiter}${yyyy}`;
};