/**
 * Get date string format DD/MM/YYYY
 * @param  {string} delimiter     The delimiter
 * @return {string}
 */
export const convertToVietnamese = (key: string): string => {
  const columnName = {
    address: "Địa chỉ",
    companyName: "Tên công ty",
    completed: "Hoàn tất",
    contactName: "Người liên hệ",
    content: "Nội dung",
    createdAt: "Ngày liên hệ",
    feedbackPOSemployee: "Nhân sự quản lý",
    feedbackPOSservice: "Thiết bị POS - Hệ thống",
    incidentTime: "Ngày hư",
    merchantName: "Tên đại lý",
    merchantNumber: "Số đại lý",
    new: "Mới",
    note: "Ghi chú",
    ownerName: "Người thực hiện",
    phone: "Số điện thoại",
    posName: "Tên máy",
    kpp: 'Kênh phân phối',
    posType: "Dòng máy POS",
    processing: "Đang xử lý",
    acbRequestotherTask: 'Tác vụ khác',
    taskName: 'Tên tác vụ',
    file: 'Tài liệu',
    acbRequestnewMerchant: 'Đăng ký ĐVCNT',
    province: "Tỉnh / Thành phố",
    rejected: "Bị từ chối",
    requestContent: "Nội dung yêu cầu/tình trạng",
    requestServiceacb2pay: "XXX2PAY",
    requestServiceacbpos: "XXX POS",
    service: "Dịch vụ",
    status: "Trạng thái",
    type: "Loại",
    updatedAt: "Ngày cập nhật"
  };
  return columnName.hasOwnProperty(key) ? columnName[key] : '';
};