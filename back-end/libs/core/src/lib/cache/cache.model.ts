import { CacheManagerOptions } from '@nestjs/common';
import { Observable } from 'rxjs';

export interface ICacheManager {
  store?: any;
  get<T>(key: string): Promise<T>;
  set<T>(key: string, value: T, options?: CacheManagerOptions): Promise<T>;
  del(key: string): Promise<void>;
}

export interface CacheOptions extends CacheManagerOptions {
  keyPrefix?: string;
  key?: string;
  keyByParams?: boolean;
  promise?: boolean;
  observable?: boolean;
}

export type Cacheable<T> = (...args) => Observable<T>;
