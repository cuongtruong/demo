import { from, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { CacheService } from '../cache.service';
import { BaseCacheResolver } from './base.resolver';
import { ResolverOptions } from './resolver.model';

export class CacheObservableResolver implements BaseCacheResolver {
  /**
   * Constructor
   */
  constructor(private cacheService: CacheService) { }

  /**
   * Resolve cache
   *
   * @param  {ResolverOptions} resolverOptions  The resolver optoins
   * @return {Observable<T>}
   */
  resolve<T>({ cacheKey, method, methodArgs, cacheOptions }: ResolverOptions) {
    const cachedData = this.cacheService.get<T>(cacheKey);

    return from(cachedData).pipe(
      switchMap(res => {
        return res
          ? of(res)
          : method(methodArgs).pipe(
            tap((methodResult: T) => this.cacheService.set<T>(cacheKey, methodResult, cacheOptions))
          );
      })
    );
  }
}