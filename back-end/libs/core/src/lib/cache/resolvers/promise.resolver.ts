import { CacheService } from '../cache.service';

import { BaseCacheResolver } from './base.resolver';
import { ResolverOptions } from './resolver.model';

export class CachePromiseResolver implements BaseCacheResolver {
  /**
   * Constructor
   */
  constructor(private cacheService: CacheService) { }

  /**
   * Resolve cache
   *
   * @param  {ResolverOptions} resolverOptions  The resolver optoins
   * @return {Promise<T>}
   */
  async resolve<T>({ cacheKey, method, methodArgs, cacheOptions }: ResolverOptions): Promise<T> {
    const cachedData = await this.cacheService.get<T>(cacheKey);

    return cachedData
      ? cachedData
      : method(methodArgs).then((methodResult: T) => {
        this.cacheService.set<T>(cacheKey, methodResult, cacheOptions);

        return methodResult;
      });
  }
}