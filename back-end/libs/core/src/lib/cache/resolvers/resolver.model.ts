import { CacheOptions } from '../cache.model';

export interface ResolverOptions {
  cacheKey: string;
  method: Function;
  methodArgs: any[];
  cacheOptions: CacheOptions;
}