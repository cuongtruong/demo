export interface BaseCacheResolver {
  resolve({ cacheKey, method, methodArgs, cacheOptions }): any;
}