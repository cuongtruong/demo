import { CacheService } from '../cache.service';

import { BaseCacheResolver } from './base.resolver';
import { ResolverOptions } from './resolver.model';

export class CacheDefaultResolver implements BaseCacheResolver {
  /**
   * Constructor
   */
  constructor(private cacheService: CacheService) { }

  /**
   * Resolve cache
   *
   * @param  {ResolverOptions} resolverOptions  The resolver optoins
   * @return {T}
   */
  async resolve<T>({ cacheKey, method, methodArgs, cacheOptions }: ResolverOptions): Promise<T> {
    const cachedData = await this.cacheService.get<T>(cacheKey);

    if (cachedData) {
      return cachedData;
    }

    const methodResult = method(methodArgs);

    this.cacheService.set<T>(cacheKey, methodResult, cacheOptions);

    return methodResult;
  }
}