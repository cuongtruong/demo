import { CacheService } from '../cache.service';

import { BaseCacheResolver } from './base.resolver';
import { CacheDefaultResolver } from './default.resolver';
import { CacheObservableResolver } from './observable.resolver';
import { CachePromiseResolver } from './promise.resolver';

export class ResolverFactory {
  /**
   * Constructor
   */
  constructor(private cacheService: CacheService) { }

  /**
   * Create Cache Resolver
   *
   * @param  {CacheOptions} options   The cache options
   * @return {BaseCacheResolver}
   */
  create({ observable, promise, returnType }
    : { observable?: boolean, promise?: boolean, returnType?: 'Promise' | 'Observable' | 'Object' | 'any' }
  ): BaseCacheResolver {
    if (observable || ['Observable', 'WrappedObservable'].includes(returnType)) {
      return new CacheObservableResolver(this.cacheService);
    }

    if (promise || ['Promise', 'WrappedPromise'].includes(returnType)) {
      return new CachePromiseResolver(this.cacheService);
    }

    return new CacheDefaultResolver(this.cacheService);
  }
}