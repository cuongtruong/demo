import { InternalServerErrorException } from '@nestjs/common';
import { tap } from 'rxjs/operators';

import { CacheOptions, Cacheable } from './cache.model';
import { CacheService } from './cache.service';
import { ResolverFactory } from './resolvers';

/**
 * Cache data
 *
 * @param  {CacheOptions} cacheOptions  The cache options
 * @return {Function}
 */
export const Cache = <T>(cacheOptions?: CacheOptions): Function => {
  return (target: any, methodName: string, descriptor: TypedPropertyDescriptor<Cacheable<T>>): TypedPropertyDescriptor<Cacheable<T>> => {
    // NOTICE: Cache Module should be import to make sure Cache Service has been already initialized
    const originalMethod = descriptor.value;
    const className = target.constructor.name;
    const returnType = (Reflect.getMetadata('design:returntype', target, methodName) || {}).name;

    descriptor.value = function (...methodArgs: any[]) {
      const cacheService = CacheService.getInstance();

      if (!cacheService || !(cacheService instanceof CacheService)) {
        throw new InternalServerErrorException('Target Class should inject CacheService');
      }
      else {
        const { keyPrefix = '', key = '', keyByParams = false, observable = false, promise = false } = cacheOptions || {};
        const cacheKey = `${keyPrefix ? `${keyPrefix}:` : ''}${
          key
          || `${!keyByParams ? `${className}:${methodName}:` : ''}${methodArgs.map(a => `${a}`).join()}`
          }`;
        const resolverFactory = new ResolverFactory(cacheService);
        const resolver = resolverFactory.create({ observable, promise, returnType });

        // return resolver.resolve({ cacheKey, method: () => originalMethod.apply(this, methodArgs), methodArgs, cacheOptions });
        return resolver.resolve({ cacheKey, method: (args) => originalMethod.apply(this, args), methodArgs, cacheOptions });
      }
    };

    return descriptor;
  };
};

/**
 * Cache buster
 *
 * @param  {string} cacheKey    The cache key
 * @return {Function}
 */
export const CacheBuster = <T>(cacheKey: string): Function => {
  return (target: any, methodName: string, descriptor: TypedPropertyDescriptor<Cacheable<T>>): TypedPropertyDescriptor<Cacheable<T>> => {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
      return originalMethod.apply(this, args).pipe(tap(this.cacheService.del(cacheKey)));
    };

    return descriptor;
  };
};