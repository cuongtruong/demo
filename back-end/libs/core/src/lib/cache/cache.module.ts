import { CacheModule as NestCacheModule, Global, Module } from '@nestjs/common';

import { CacheConfigService } from './cache-config.service';
import { CacheService } from './cache.service';

@Global()
@Module({
  imports: [
    NestCacheModule.registerAsync({
      useClass: CacheConfigService,
      inject: [CacheConfigService]
    }),
  ],
  providers: [
    CacheConfigService,
    CacheService,
    // // Global cache
    // {
    //   provide: APP_INTERCEPTOR,
    //   useClass: CacheInterceptor,
    // },
  ],
  exports: [NestCacheModule, CacheService],
})
export class CacheModule {
  constructor(private readonly cacheService: CacheService) { }
}