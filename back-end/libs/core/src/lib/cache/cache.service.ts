import { CACHE_MANAGER, CacheManagerOptions, Inject, Injectable } from '@nestjs/common';

import { ICacheManager } from './cache.model';

/**
 * Singleton Cache Service
 */
@Injectable()
export class CacheService {
  static instance = null;
  private cache!: ICacheManager;

  /**
   * Constructor
   */
  constructor(@Inject(CACHE_MANAGER) cache: ICacheManager) {
    if (!CacheService.instance) {
      CacheService.instance = this;
    }

    this.cache = cache;

    return CacheService.instance;
  }

  /**
   * Get class instance
   *
   * @return {type} {description}
   */
  static getInstance(): CacheService {
    return CacheService.instance;
  }

  /**
   * Get value by key
   *
   * @param  {string} key   The cache key
   * @return {T}            The info
   */
  get<T>(key: string): Promise<T> {
    return this.cache.get(key);
  }

  /**
   * Set value
   *
   * @param  {string} key   The cache key
   * @return {T}            The info
   */
  set<T>(key: string, value: T, options?: CacheManagerOptions): Promise<T> {
    return this.cache.set(key, value, options);
  }

  /**
   * Delete cache by key
   *
   * @param  {string} key   The cache key
   * @return {void}
   */
  del(key: string): Promise<void> {
    return this.cache.del(key);
  }
}