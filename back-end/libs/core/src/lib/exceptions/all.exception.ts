import { ArgumentsHost, Catch, HttpServer, HttpStatus, HttpException } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';

import { ConfigService } from '@xxx/config';

@Catch(HttpException)
export class AllExceptionsFilter extends BaseExceptionFilter {
  /**
   * Constructor
   */
  constructor(applicationRef?: HttpServer<any, any>) {
    super(applicationRef);
  }

  catch(exception: any, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse();
    const request = context.getRequest();
    const { referer } = request.headers;
    const status = exception.getStatus ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

    if (!ConfigService.isProd) {
      console.error(exception);
    }

    ConfigService.error('Exceptions', exception, { status, message: exception.message, referer, path: request.url });

    return response
      .status(status)
      .json({
        statusCode: exception?.response?.statusCode || status,
        statusMessage:  exception?.response?.message || exception.message,
        message: exception?.response?.error || exception.message,
        path: request.url,
      });
  }
}