import { SetMetadata } from '@nestjs/common';

export const DECORATOR_TOKEN_ROLES = 'roles';

export const Roles = (...roles: string[]) => SetMetadata(DECORATOR_TOKEN_ROLES, roles);