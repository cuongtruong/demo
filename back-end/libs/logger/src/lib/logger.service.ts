import { Injectable } from '@nestjs/common';
import { Logger, createLogger, format, transports as winstonTransports } from 'winston';
import * as DailyRotateFile from 'winston-daily-rotate-file';

import { DEFAULT_LOGGER_OPTIONS, LoggerOptions, LoggerTransport } from './logger.model';

@Injectable()
export class LoggerService {
  private logger: Logger;
  private requestId: string;
  private context: string;

  constructor(options: Partial<LoggerOptions>) {
    this.buildLogger({ ...DEFAULT_LOGGER_OPTIONS, ...options });
  }

  /**
   * Set request id
   *
   * @param  {string} id    The request id
   * @return {void}
   */
  setRequestId(id: string): void {
    this.requestId = id;
  }

  /**
   * Get request id
   *
   * @return {string}
   */
  getRequestId(): string {
    return this.requestId;
  }

  /**
   * Set context
   *
   * @param  {string} ctx    The context
   * @return {void}
   */
  setContext(ctx: string): void {
    this.context = ctx;
  }

  /**
   * Get context
   *
   * @return {string}
   */
  getContext(): string {
    return this.context;
  }

  /**
   * Log message as info
   *
   * @param  {any} message        The message
   * @param  {string?} context    The context
   * @return {void}
   */
  log(message: any, context?: string): void {
    this.info(message, context);
  }

  /**
   * Log message as debug
   *
   * @param  {any} message        The message
   * @param  {string?} context    The context
   * @return {void}
   */
  debug(message: any, context?: string): void {
    this.logger.debug(message, [{ context, reqId: this.requestId }]);
  }

  /**
   * Log message as info
   *
   * @param  {any} message        The message
   * @param  {string?} context    The context
   * @return {void}
   */
  info(message: any, context?: string): void {
    this.logger.info(message, [{ context, reqId: this.requestId }]);
  }

  /**
   * Log message as warning
   *
   * @param  {any} message        The message
   * @param  {string?} context    The context
   * @return {void}
   */
  warn(message: any, context?: string): void {
    this.logger.warn(message, [{ context, reqId: this.requestId }]);
  }

  /**
   * Log message as error
   *
   * @param  {any} message        The message
   * @param  {string?} trace      The trace message
   * @param  {string?} context    The context
   * @return {void}
   */
  error(message: any, trace?: string, context?: string): void {
    this.logger.error(message, [{ context }]);
    // this.logger.error(trace, [{ context, reqId: this.requestId }]);
  }

  /**
   * Build logger
   *
   * @param  {any} options      The logger options
   * @return {void}
   */
  protected buildLogger(
    { loggers, path, serviceName, fileDatePattern, zippedArchive, maxFiles, level, timeFormat }: Partial<LoggerOptions>
  ): void {
    const transports = [];

    if (loggers) {
      if (loggers.includes(LoggerTransport.CONSOLE)) {
        transports.push(new winstonTransports.Console());
      }

      if (loggers.includes(LoggerTransport.ROTATE)) {
        const rotateLogger = new DailyRotateFile({
          filename: `${path}/${serviceName}-%DATE%.log`,
          datePattern: fileDatePattern,
          zippedArchive,
          maxFiles,
          options: { flags: 'a', mode: '0776' },
        });
        transports.push(rotateLogger);
      }
    }

    this.logger = createLogger({
      level,
      transports,
      format: format.combine(
        format.timestamp({ format: timeFormat }),
        this.getLoggerFormat()
      )
    });
  }

  /**
   * Get logger format
   *
   * @return {Format}
   */
  protected getLoggerFormat(): any {
    return format.printf((info) => {
      const { '0': { context, reqId }, level, message: rawMessage, timestamp } = info;
      let message = rawMessage;

      if ((Array.isArray(message) && !message.length)) {
        message = null;
      }

      return `${timestamp} [${level.toUpperCase()}] ${context}${message ? `: ${JSON.stringify(message)}` : ''}`;
    });
  }
}
