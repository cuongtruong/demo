export enum LoggerTransport {
  CONSOLE = 'console',
  ROTATE = 'rotate'
}

export interface LoggerOptions {
  level: string;
  serviceName: string;
  loggers: LoggerTransport[];
  path?: string;
  timeFormat: string;
  fileDatePattern: string;
  maxFiles: string;
  zippedArchive: boolean;
}

export const DEFAULT_LOGGER_OPTIONS: LoggerOptions = {
  level: 'info',
  serviceName: 'MerchantPortal',
  loggers: [LoggerTransport.CONSOLE],
  timeFormat: 'YYYY-MM-DD HH:mm:ss',
  fileDatePattern: 'YYYY-MM-DD',
  maxFiles: '30d',
  zippedArchive: false
};