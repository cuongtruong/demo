import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';

import { ConfigService } from '@xxx/config';

import { Feature, User } from './entities';

const { type, host, port, username, password, database, synchronize, schema, logging, cache } = ConfigService.dbOptions;
const dbOptions = {
  type, host, port, username, password, database, synchronize, schema, logging, cache,
  entities: [Feature, User],
  subscribers: []
} as TypeOrmModuleOptions;

ConfigService.log('Database Connections', JSON.stringify({ type, host, port, username, database }));

@Module({
  imports: [
    TypeOrmModule.forRoot(dbOptions),
  ],
  providers: [],
  exports: [],
})
export class DatabaseModule { }
