import { MigrationInterface, QueryRunner } from "typeorm";

export class AddTableFile1584774755340 implements MigrationInterface {
  name = 'AddTableFile1584774755340'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE "public"."file" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "originalname" character varying NOT NULL, "filename" character varying NOT NULL, "mimetype" character varying NOT NULL, "destination" character varying NOT NULL, "size" character varying NOT NULL, "path" character varying NOT NULL, CONSTRAINT "PK_bf2f5ba5aa6e3453b04cb4e4720" PRIMARY KEY ("id"))`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "public"."file"`, undefined);
  }
}
