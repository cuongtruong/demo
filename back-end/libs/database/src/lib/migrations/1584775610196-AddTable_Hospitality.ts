import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddTableHospitality1584775610196 implements MigrationInterface {
  name = 'AddTableHospitality1584775610196'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('CREATE TABLE "public"."hospitality" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "createdBy" character varying, "createdById" character varying, "updatedBy" character varying, "updatedById" character varying, "name" character varying NOT NULL, "nameVi" character varying NOT NULL, "description" character varying NOT NULL, "descriptionVi" character varying NOT NULL, "address" character varying NOT NULL, "addressVi" character varying NOT NULL, "city" character varying NOT NULL, "cityVi" character varying NOT NULL, "phone" character varying NOT NULL, "phoneVi" character varying NOT NULL, "amenities" character varying, "amenitiesVi" character varying, "partnerInfo" character varying, "partnerInfoVi" character varying, "validFrom" TIMESTAMP WITH TIME ZONE, "validTo" TIMESTAMP WITH TIME ZONE, "availability" integer NOT NULL DEFAULT 0, "adults" integer NOT NULL DEFAULT 0, "children" integer NOT NULL DEFAULT 0, "type" character varying NOT NULL, "active" boolean NOT NULL DEFAULT true, "longitude" character varying NOT NULL, "latitude" character varying NOT NULL, "fileIds" jsonb, CONSTRAINT "PK_a47554dcf0d8f8d4c4546589eea" PRIMARY KEY ("id"))', undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DROP TABLE "public"."hospitality"', undefined);
  }
}
