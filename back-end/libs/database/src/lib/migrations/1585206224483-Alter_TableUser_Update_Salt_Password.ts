import {MigrationInterface, QueryRunner} from 'typeorm';

export class AlterTableUserUpdateSaltPassword1585206224483 implements MigrationInterface {
	name = 'AlterTableUserUpdateSaltPassword1585206224483'

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_287d3b25c382e52036f4979db27"', undefined);
		await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2"', undefined);
		await queryRunner.query('ALTER TABLE "public"."user" ALTER COLUMN "salt" DROP NOT NULL', undefined);
		await queryRunner.query('ALTER TABLE "public"."user" ALTER COLUMN "passwordHash" DROP NOT NULL', undefined);
		await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_287d3b25c382e52036f4979db27" FOREIGN KEY ("userId") REFERENCES "public"."user"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
		await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2" FOREIGN KEY ("featureId") REFERENCES "public"."feature"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2"', undefined);
		await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_287d3b25c382e52036f4979db27"', undefined);
		await queryRunner.query('ALTER TABLE "public"."user" ALTER COLUMN "passwordHash" SET NOT NULL', undefined);
		await queryRunner.query('ALTER TABLE "public"."user" ALTER COLUMN "salt" SET NOT NULL', undefined);
		await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2" FOREIGN KEY ("featureId") REFERENCES "public"."feature"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
		await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_287d3b25c382e52036f4979db27" FOREIGN KEY ("userId") REFERENCES "public"."user"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
	}
}
