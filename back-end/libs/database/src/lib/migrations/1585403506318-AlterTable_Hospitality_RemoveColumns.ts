import {MigrationInterface, QueryRunner} from 'typeorm';

export class AlterTableHospitalityRemoveColumns1585403506318 implements MigrationInterface {
    name = 'AlterTableHospitalityRemoveColumns1585403506318'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_287d3b25c382e52036f4979db27"', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" DROP COLUMN "longitude"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" DROP COLUMN "latitude"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" DROP COLUMN "fileIds"', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_287d3b25c382e52036f4979db27" FOREIGN KEY ("userId") REFERENCES "public"."user"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2" FOREIGN KEY ("featureId") REFERENCES "public"."feature"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2"', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_287d3b25c382e52036f4979db27"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" ADD "fileIds" jsonb', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" ADD "latitude" character varying NOT NULL', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" ADD "longitude" character varying NOT NULL', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2" FOREIGN KEY ("featureId") REFERENCES "public"."feature"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_287d3b25c382e52036f4979db27" FOREIGN KEY ("userId") REFERENCES "public"."user"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
    }

}
