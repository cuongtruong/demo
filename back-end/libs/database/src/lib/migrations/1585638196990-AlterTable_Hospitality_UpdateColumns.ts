import {MigrationInterface, QueryRunner} from 'typeorm';

export class AlterTableHospitalityUpdateColumns1585638196990 implements MigrationInterface {
    name = 'AlterTableHospitalityUpdateColumns1585638196990'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_287d3b25c382e52036f4979db27"', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" DROP COLUMN "description"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" DROP COLUMN "descriptionVi"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" ADD "promotions" character varying', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" ADD "promotionsVi" character varying', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" ADD "fileIds" jsonb', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_287d3b25c382e52036f4979db27" FOREIGN KEY ("userId") REFERENCES "public"."user"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2" FOREIGN KEY ("featureId") REFERENCES "public"."feature"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2"', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_287d3b25c382e52036f4979db27"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" DROP COLUMN "fileIds"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" DROP COLUMN "promotionsVi"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" DROP COLUMN "promotions"', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" ADD "descriptionVi" character varying NOT NULL', undefined);
        await queryRunner.query('ALTER TABLE "public"."hospitality" ADD "description" character varying NOT NULL', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2" FOREIGN KEY ("featureId") REFERENCES "public"."feature"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
        await queryRunner.query('ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_287d3b25c382e52036f4979db27" FOREIGN KEY ("userId") REFERENCES "public"."user"("id") ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
    }

}
