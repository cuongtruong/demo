/* eslint-disable quotes */
import { MigrationInterface, QueryRunner } from 'typeorm';

export class Initialize1584357157172 implements MigrationInterface {
  name = 'Initialize1584357157172'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE "public"."feature" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "code" character varying NOT NULL, "name" character varying NOT NULL, "description" character varying, CONSTRAINT "UQ_81f2d0e68562d6efc515696b811" UNIQUE ("code"), CONSTRAINT "PK_7ee173ef61740b68f78d78055e2" PRIMARY KEY ("id"))`, undefined);
    await queryRunner.query(`CREATE INDEX "IDX_81f2d0e68562d6efc515696b81" ON "public"."feature" ("code") `, undefined);
    await queryRunner.query(`CREATE TABLE "public"."user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "createdBy" character varying, "createdById" character varying, "updatedBy" character varying, "updatedById" character varying, "username" character varying NOT NULL, "role" character varying NOT NULL, "name" character varying NOT NULL, "avatar" character varying, "active" boolean NOT NULL DEFAULT true, "salt" character varying NOT NULL, "passwordHash" character varying NOT NULL, "provider" character varying, "externalId" character varying, "validFrom" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, CONSTRAINT "UQ_b67337b7f8aa8406e936c2ff754" UNIQUE ("username"), CONSTRAINT "PK_03b91d2b8321aa7ba32257dc321" PRIMARY KEY ("id"))`, undefined);
    await queryRunner.query(`CREATE INDEX "IDX_b67337b7f8aa8406e936c2ff75" ON "public"."user" ("username") `, undefined);
    await queryRunner.query(`CREATE INDEX "IDX_ffb985d06b9a1071ccce09c203" ON "public"."user" ("externalId") `, undefined);
    await queryRunner.query(`CREATE TABLE "public"."user_feature" ("userId" uuid NOT NULL, "featureId" uuid NOT NULL, CONSTRAINT "PK_f554c5dc3eada998e26e2aad368" PRIMARY KEY ("userId", "featureId"))`, undefined);
    await queryRunner.query(`CREATE INDEX "IDX_287d3b25c382e52036f4979db2" ON "public"."user_feature" ("userId") `, undefined);
    await queryRunner.query(`CREATE INDEX "IDX_60bbb47be2d9acc2cc0db7ec0e" ON "public"."user_feature" ("featureId") `, undefined);
    await queryRunner.query(`ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_287d3b25c382e52036f4979db27" FOREIGN KEY ("userId") REFERENCES "public"."user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    await queryRunner.query(`ALTER TABLE "public"."user_feature" ADD CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2" FOREIGN KEY ("featureId") REFERENCES "public"."feature"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);

    // Seed data
    await queryRunner.query(`
      -- Features
      INSERT INTO public.feature (id, "createdAt", "updatedAt", code, name, description) VALUES ('796e3b55-5461-4541-80b4-d5ad1aefa11c', '2019-09-09 09:09:09.194438+07', '2019-09-09 09:09:09.194438+07', 'manageUsers', 'Manage Users', 'Allows user to manage users');
      INSERT INTO public.feature (id, "createdAt", "updatedAt", code, name, description) VALUES ('8c58f46a-d9af-4f1c-b4e6-b45b202f5875', '2019-09-09 09:09:09.194438+07', '2019-09-09 09:09:09.194438+07', 'blueRewards', 'Blue Rewards', 'Allows ACB user to view all Blue Rewards info');

      -- Users
      INSERT INTO public.user (id, "createdAt", "updatedAt", "createdBy", "updatedBy", username, role, "name", active, salt, "passwordHash", "validFrom") VALUES ('6dbd26e2-65c0-11ea-bc55-0242ac130003', '2019-09-09 09:09:09.213851+07', '2019-09-09 09:09:09.213851+07', 'System Admin', 'System Admin', 'xxx', 'sysadmin', 'System Admin', true, '$2a$10$e26Os2TchEvpm01VPA1Uyu', '$2a$10$e26Os2TchEvpm01VPA1UyuxNReCwjRoqGKWW7o2SWuelz.sjjWqze', '2019-09-09 09:09:09.213851+07');

      -- Grant permissions
      INSERT INTO public.user_feature ("userId", "featureId") VALUES ('6dbd26e2-65c0-11ea-bc55-0242ac130003', '796e3b55-5461-4541-80b4-d5ad1aefa11c');
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_60bbb47be2d9acc2cc0db7ec0e2"`, undefined);
    await queryRunner.query(`ALTER TABLE "public"."user_feature" DROP CONSTRAINT "FK_287d3b25c382e52036f4979db27"`, undefined);
    await queryRunner.query(`DROP INDEX "public"."IDX_60bbb47be2d9acc2cc0db7ec0e"`, undefined);
    await queryRunner.query(`DROP INDEX "public"."IDX_287d3b25c382e52036f4979db2"`, undefined);
    await queryRunner.query(`DROP TABLE "public"."user_feature"`, undefined);
    await queryRunner.query(`DROP INDEX "public"."IDX_ffb985d06b9a1071ccce09c203"`, undefined);
    await queryRunner.query(`DROP INDEX "public"."IDX_b67337b7f8aa8406e936c2ff75"`, undefined);
    await queryRunner.query(`DROP TABLE "public"."user"`, undefined);
    await queryRunner.query(`DROP INDEX "public"."IDX_81f2d0e68562d6efc515696b81"`, undefined);
    await queryRunner.query(`DROP TABLE "public"."feature"`, undefined);
  }
}
