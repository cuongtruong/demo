// import { EntitySubscriberInterface, EventSubscriber, InsertEvent } from 'typeorm';

// // It should be absolute import for TypeORM to process migrations
// // tslint:disable-next-line: nx-enforce-module-boundaries
// import { RequestContext } from '../../../../core/src/lib/context/request-context.model';


// @EventSubscriber()
// export class BaseSubscriber implements EntitySubscriberInterface {
//   /**
//    * Called before entity insertion.
//    */
//   beforeInsert(event: InsertEvent<any>) {
//     const entity = event.entity;
//     const currentUser = RequestContext.currentUser;

//     if (currentUser) {
//       entity['createdBy'] = entity['updatedBy'] = currentUser.name;
//       entity['createdById'] = entity['updatedById'] = currentUser.id;
//     }
//   }

//   /**
//    * Called before entity insertion.
//    */
//   beforeUpdate(event: InsertEvent<any>) {
//     const entity = event.entity;
//     const currentUser = RequestContext.currentUser;

//     if (currentUser) {
//       entity['updatedBy'] = currentUser.name;
//       entity['updatedById'] = currentUser.id;
//     }
//   }
// }