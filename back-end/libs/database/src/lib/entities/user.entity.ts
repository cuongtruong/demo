import { Exclude, Type } from 'class-transformer';
import { Column, Entity, Index, JoinTable, ManyToMany, Unique } from 'typeorm';

import { AuditableEntity } from './base.entity';
import { Feature } from './feature.entity';

export enum Role {
  sysadmin = 'sysadmin',
  admin = 'admin',
  user = 'user'
}

export enum AuthProvider {
  phone = 'phone',
  email = 'email',
  facebook = 'facebook',
  appleId = 'appleId',
  persNbr = 'persNbr',
}

export const ADMINISTRATOR_ROLES = [Role.sysadmin, Role.admin];

@Unique(['username'])
@Entity()
export class User extends AuditableEntity {
  @Index()
  @Column()
  username: string;

  @Column()
  role: Role;

  @Column()
  name: string;

  @Column({ nullable: true })
  avatar: string;

  @Column({ default: true })
  active: boolean;

  @Exclude()
  @Column({ nullable: true })
  salt: string;

  @Exclude()
  @Column({ nullable: true })
  passwordHash: string;

  @Column({ nullable: true })
  provider: AuthProvider;

  @Column({ nullable: true })
  @Index()
  externalId: string;

  @Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  validFrom: Date;

  // /**
  //  * [class-transformer]: Since Typescript does not have good reflection abilities yet,
  //  * we should implicitly specify what type of object each property contain.
  //  * This is done using @Type decorator
  //  */
  // @Type(() => Feature)
  // @ManyToMany(() => Feature)
  // @JoinTable({
  //   name: 'user_feature',
  //   joinColumn: { name: 'userId', referencedColumnName: 'id' },
  //   inverseJoinColumn: { name: 'featureId', referencedColumnName: 'id' }
  // })
  @ManyToMany(() => Feature)
  @JoinTable({ name: 'user_feature' })
  features: Feature[];

  /**
   * Constructor
   *
   * @param  {Partial<User> } user    The partial user info
   */
  constructor(user: Partial<User>) {
    super();

    Object.assign(this, user || {});
  }
}
