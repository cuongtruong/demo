import { Column, Entity, Index, Unique } from 'typeorm';

import { BaseEntity } from './base.entity';

@Unique(['code'])
@Entity()
export class Feature extends BaseEntity {
  @Index()
  @Column()
  code: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  description: string;
}