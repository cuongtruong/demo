--======================================================
--======================================================

-- NOTICE!!!
-- PLEASE READ BEFORE RUNNING THIS SCRIPT!!!!
-- You must create new user in postgresql

--======================================================
--======================================================
-- User: lsadmin
-- DROP USER lsadmin;

CREATE USER demoadmin WITH
  LOGIN
  SUPERUSER
  CREATEDB
  CREATEROLE
  INHERIT
  NOREPLICATION
  CONNECTION LIMIT -1
  PASSWORD '(3McstX//2+kbg<j';
