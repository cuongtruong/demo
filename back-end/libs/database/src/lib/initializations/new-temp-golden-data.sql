-- Features
INSERT INTO public.feature (id, "createdAt", "updatedAt", code, name, description) VALUES ('796e3b55-5461-4541-80b4-d5ad1aefa11c', '2019-09-09 09:09:09.194438+07', '2019-09-09 09:09:09.194438+07', 'manageUsers', 'Manage Users', 'Allows user to manage users');
INSERT INTO public.feature (id, "createdAt", "updatedAt", code, name, description) VALUES ('8c58f46a-d9af-4f1c-b4e6-b45b202f5875', '2019-09-09 09:09:09.194438+07', '2019-09-09 09:09:09.194438+07', 'audit', 'History', 'Allows user to view all historical logs');

-- Users
INSERT INTO public.user (id, "createdAt", "updatedAt", "createdBy", "updatedBy", username, role, "firstName", "lastName", active, salt, "passwordHash") VALUES ('6dbd26e2-65c0-11ea-bc55-0242ac130003', '2019-09-09 09:09:09.213851+07', '2019-09-09 09:09:09.213851+07', 'System Admin', 'System Admin', 'sysadmin', 'sysadmin', 'System', 'Admin', true, '$2a$10$VC50JKJBQt2c7vadQjK0le', '$2a$10$VC50JKJBQt2c7vadQjK0leRnUqV/WeEkEwKLAAKjJiOlg1jzepXom');
INSERT INTO public.user (id, "createdAt", "updatedAt", "createdBy", "updatedBy", username, role, "firstName", "lastName", active, salt, "passwordHash") VALUES ('3e55e7a4-5043-4693-9102-49c98496c5d2', '2019-09-09 09:09:09.213851+07', '2019-09-09 09:09:09.213851+07', 'System Admin', 'System Admin', 'user1', 'user', 'User', '#1', true, '$2b$10$7P7Tkxcbnt7sAYf.GiRYq.', '$2b$10$7P7Tkxcbnt7sAYf.GiRYq.OUem8aNY0.BuTD50EkY8iElAq0kEIvW');
INSERT INTO public.user (id, "createdAt", "updatedAt", "createdBy", "updatedBy", username, role, "firstName", "lastName", active, salt, "passwordHash") VALUES ('851279af-25fd-4fc9-a69a-9985f5bf42a1', '2019-09-09 09:09:09.213851+07', '2019-09-09 09:09:09.213851+07', 'System Admin', 'System Admin', 'user2', 'user', 'User', '#2', false, '$2b$10$7P7Tkxcbnt7sAYf.GiRYq.', '$2b$10$7P7Tkxcbnt7sAYf.GiRYq.OUem8aNY0.BuTD50EkY8iElAq0kEIvW');

-- Grant permissions
INSERT INTO public.user_feature ("userId", "featureId") VALUES ('6dbd26e2-65c0-11ea-bc55-0242ac130003', '796e3b55-5461-4541-80b4-d5ad1aefa11c');
INSERT INTO public.user_feature ("userId", "featureId") VALUES ('6dbd26e2-65c0-11ea-bc55-0242ac130003', '8c58f46a-d9af-4f1c-b4e6-b45b202f5875');
INSERT INTO public.user_feature ("userId", "featureId") VALUES ('3e55e7a4-5043-4693-9102-49c98496c5d2', '8c58f46a-d9af-4f1c-b4e6-b45b202f5875');
INSERT INTO public.user_feature ("userId", "featureId") VALUES ('3e55e7a4-5043-4693-9102-49c98496c5d2', '796e3b55-5461-4541-80b4-d5ad1aefa11c');
INSERT INTO public.user_feature ("userId", "featureId") VALUES ('851279af-25fd-4fc9-a69a-9985f5bf42a1', '8c58f46a-d9af-4f1c-b4e6-b45b202f5875');

-- Example to truncate and re-populate data using csv file
-- Truncate all data
TRUNCATE TABLE branch;

-- Populate new data
COPY branch(name, code) 
FROM '/path-to-file/03-golden-data-branch-XXX.csv'
DELIMITER ','
CSV HEADER;
